module.exports = {
  content: [
    './public/index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        pink: {
          40: '#FFFBFB',
          60: '#FFEEF3',
          150: '#FFCCDB',
          250: '#FC5285'
        },
        gray: {
          40: '#F9F9F9',
          150: '#E5E5E5',
          250: '#52575C',
          350: '#7D8491'
        },
        black:{
          150: '#1E1E1E',
          250: '#25282B',
        }
      },
      dropShadow: {
        '3xl': '0px 12px 26px rgba(198, 163, 175, 0.15)',
        '4xl': '0px 4px 40px rgba(249, 91, 133, 0.15);',
            
           },

      zIndex: {
        '-1': '-1'
      },
      flexGrow: {
        5: '5'
      },
      maxHeight: {
        'screen-menu': 'calc(100vh - 3.5rem)',
        modal: 'calc(100vh - 160px)'
      },
      transitionProperty: {
        position: 'right, left, top, bottom, margin, padding',
        textColor: 'color'
      },
      keyframes: {
        fadeOut: {
          from: { opacity: 1 },
          to: { opacity: 0 }
        },
        fadeIn: {
          from: { opacity: 0 },
          to: { opacity: 1 }
        }
      },
      animation: {
        fadeOut: 'fadeOut 250ms ease-in-out',
        fadeIn: 'fadeIn 250ms ease-in-out'
      }
    }
  },
  plugins: [
    require('@tailwindcss/forms')
  ]
}
