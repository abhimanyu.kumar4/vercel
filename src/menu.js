import {
  mdiAccountCircle,
  mdiDesktopMac,
  mdiGithub,
  mdiLock,
  mdiAlertCircle,
  mdiMonitorShimmer,
  mdiSquareEditOutline,
  mdiTable,
  mdiViewList,
  mdiTelevisionGuide,
  mdiResponsive,
  mdiPalette,mdiHospitalBuilding,mdiHelpCircle,mdiDesk,
  mdiBloodBag,mdiHumanMale,mdiViewQuilt, 
} from '@mdi/js'

export default [
  // 'General',
  [
    {
      to: '/dashboard',
      icon: mdiViewQuilt ,
      label: 'Dashboard'
    },
    {
      to: '/hospital-list',
      icon: mdiHospitalBuilding,
      label: 'All Hospitals'
    },
    
  ],
  // 'Examples',
  [
    {
      
      label: 'Help',
      icon: mdiHelpCircle
    },
  ]
  ]
