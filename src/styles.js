const bodyBase = 'text-base dark:bg-gray-800 dark:text-gray-100'

export const basic = {
  html: 'scrollbars-light',
  body: `bg-white ${bodyBase}`,
  lightBorder: 'border-gray-100',
  lightBg: 'bg-gray-100',
  aside: 'bg-white border-r border-gray-100',
  asideBrand: '',
  asideMenuCloseLg: '',
  asideMenuLabel: 'dark:text-gray-400',
  asideMenuItem: 'text-blue-600 hover:text-black dark:text-white',
  asideMenuItemActive: 'font-bold text-pink-250 dark:text-white',
  asideMenuItemInactive: 'text-gray-250 font-semibold',
  asideSubmenuList: 'bg-gray-50',
  navBarItemLabel: 'text-blue-600',
  navBarItemLabelHover: 'hover:text-black',
  navBarItemLabelActiveColor: 'text-black',
  navBarMenuListUpperLabel: 'bg-gray-50',
  tableTr: 'lg:hover:bg-gray-100',
  tableTrOdd: 'lg:bg-gray-50',
  overlay: 'from-white via-gray-100 to-white'
}

export const white = {
  html: 'scrollbars-light',
  body: `bg-white ${bodyBase}`,
  lightBorder: 'border-gray-100',
  lightBg: 'bg-gray-100',
  aside: 'bg-white border-r border-gray-100',
  asideBrand: '',
  asideMenuCloseLg: '',
  asideMenuLabel: 'dark:text-gray-400',
  asideMenuItem: 'text-blue-600 hover:text-black dark:text-white',
  asideMenuItemActive: 'font-bold text-pink-250 dark:text-white',
  asideMenuItemInactive: 'text-gray-250 font-semibold',
  asideSubmenuList: 'bg-gray-50',
  navBarItemLabel: 'text-blue-600',
  navBarItemLabelHover: 'hover:text-black',
  navBarItemLabelActiveColor: 'text-black',
  navBarMenuListUpperLabel: 'bg-gray-50',
  tableTr: 'lg:hover:bg-gray-100',
  tableTrOdd: 'lg:bg-gray-50',
  overlay: 'from-white via-gray-100 to-white'
}
