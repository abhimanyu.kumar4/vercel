import http from "./common";

class Staff {
  async get({
    instituteId: instituteId,
    searchText = "",
    orderBy = "",
    orderIn = 1,
    page = 1,
    limit = 6,
  }) {
    const { data } = await http.get(`/supreme/staff/v1/list/${instituteId}`, {
      params: { search: searchText, orderBy,orderIn, page: page, limit },
    });
    return data;
  }
  async getSingle(instituteId, staffId) {
    const { data } = await http.get(
      `/supreme/staff/v1/single/${instituteId}/${staffId}`
    );
    return data;
  }
  async create(staff) {
    const { data } = await http.post("/supreme/staff/v1", staff);
    return data;
  }
  async assignInstitute(staffInstitute) {
    const { data } = await http.post(
      "/supreme/staff/v1/assign-institute",
      staffInstitute
    );
    return data;
  }
  async update(staffData) {
    console.log("staffDta", staffData);
    const { data } = await http.post("/supreme/staff/v1", { ...staffData });
    return data;
  }
  async sendPassword(instituteId, staffId) {
    const { data } = await http.get(
      `/supreme/staff/v1/send-reset-password/${instituteId}/${staffId}`
    );
    return data;
  }
  async resetPassword(staffPass) {
    const { data } = await http.post("/supreme/staff/v1/staff-rest-password",{...staffPass});
    return data;
  }
  async delete(instituteId, staffId) {
    const { data } = await http.delete(
      `/supreme/staff/v1/delete/${instituteId}/${staffId}`
    );
    console.log("data", data);
    return data;
  }
  async statusUpdate(instituteId, staffId) {
    const { data } = await http.get(
      `/supreme/staff/v1/toggle-status/${instituteId}/${staffId}`
    );
    return data;
  }
  async adminStatus(instituteId, staffId) {
    const { data } = await http.get(
      `/supreme/staff/v1/toggle-admin-status/${instituteId}/${staffId}`
    );
    return data;
  }
  async dutyStatus(instituteId, staffId) {
    const { data } = await http.get(
      `/supreme/staff/v1/toggle-duty-status/${instituteId}/${staffId}`
    );
    return data;
  }
  deleteAll() {
    return http.delete(`/supreme/staff/v1`);
  }
  findByTitle(title) {
    return http.get(`/supreme/staff/v1?title=${title}`);
  }
  async csvUpload(formData) {
    const { data } = await http.post("/supreme/staff/v1/add-staff-csv", formData);
    console.log("data", data);
    return data;
  }
}
export default new Staff();
