
import axios from 'axios'
import store from '../store'
// const API_URL = "http://localhost:4000/api-v2022";
const API_URL = "https://app.lcgapp.online/api-v2022";
const apiClient = axios.create({
    withCredentials: false, // This is the default
    baseURL: API_URL,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
});

apiClient.interceptors.request.use(function (config) {
    // Do something before request is sent


   
    let authKey = store.state.authKey;
    if(authKey){
      config.headers["Authorization"] = "Bearer " + authKey;
    }
    else{
      let token = localStorage.getItem('lcg_token');
      config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
});

export default apiClient;

