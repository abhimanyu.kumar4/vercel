import http from "./common";
import { useRouter } from "vue-router";
import { useStore } from "vuex";
import formDataApi from "./formData";

class Institute {
  async getAll({ searchText = "", page = 1, orderBy = "name", orderIn = 1 }) {
    console.log("searchText", searchText);
    console.log("page", page);
    console.log("orderBy", orderBy);
    console.log("orderIn", orderIn);
    const { data } = await http.get("/supreme/institute/v1/list", {
      params: { search: searchText, page: page, orderBy, orderIn },
    });
    console.log("data", data);
    return data;
  }
  async getDashbaord(instituteId) {
    const { data } = await http.get(
      `/supreme/institute/v1/dashboard-data/${instituteId}`
    );
    console.log("data", data);
    return data;
  }
  async get(instituteId) {
    console.log("id", instituteId);
    const { data } = await http.get(
      `/supreme/institute/v1/single/${instituteId}`
    );
    console.log("data", data);
    return data;
  }
  async create(formData) {
    const { data } = await http.post("/supreme/institute/v1", formData);
    console.log("data", data);
    return data;
  }
  async toggleStatus(instituteId) {
    const { data } = await http.get(
      `/supreme/institute/v1/toggle-status/${instituteId}`
    );
    return data;
  }
  async update(formData) {
    const { data } = await formDataApi.post(`/supreme/institute/v1`, formData);
    return data;
  }
  async delete(instituteId) {
    const { data } = await http.delete(
      `/supreme/institute/v1/delete/${instituteId}`
    );
    console.log("data", data);
    return data;
  }

  deleteAll() {
    return http.delete(`/supreme/institute/v1`);
  }
  findByTitle(title) {
    return http.get(`/supreme/institute/v1?title=${title}`);
  }
}
export default new Institute();
