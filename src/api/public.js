import http from "./common";

class Public {
  async get() {
    const { data } = await http.get("/public/v1/states");
    console.log("data", data);
    return data;
  }
  async getAll(state) {
    const { data } = await http.get(`/public/v1/districts/${state}`);
    console.log("dist", data);
    return data;
  }
  async getMedical(){
    const {data} = await http.get("public/v1/medical-councils");
    console.log("data", data);
    return data;
  }
}
export default new Public();
