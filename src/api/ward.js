import http from "./common";

class Ward {
  async getAll({instituteId: instituteId, searchText='',orderBy='', page=1,limit=6}) {
    const { data } = await http.get(`/supreme/ward/v1/list/${instituteId}`, {
      params: { search: searchText,orderBy, page: page,limit },
    });
    console.log("data", data);
    return data;
  }

  async create(ward) {
    const { data } = await http.post("/supreme/ward/v1", ward);
    console.log("dataWard", data);
    return data;
  }
  async update(instituteId, wardId,name) {
    const { data } = await http.post("/supreme/ward/v1",{instituteId,wardId,name});
    return data;
  }
  async delete(instituteId,wardId) {
    const { data } = await http.delete(`/supreme/ward/v1/delete/${instituteId}/${wardId}`);
    return data;
  }
  deleteAll() {
    return http.delete(`/supreme/ward/v1`);
  }
  findByTitle(title) {
    return http.get(`/supreme/ward/v1?title=${title}`);
  }
}
export default new Ward();
