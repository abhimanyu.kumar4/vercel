import http from "./common";
import formDataApi from "./formData";

class Account {
  // get admin profile
  async get() {
    const {data} = await http.get("/supreme/account/v1/profile");
    return data;
  }
  // update admin profile
  async update(formData) {
    const {data} = await formDataApi.post(`/supreme/account/v1/profile`,formData);
    return data;
  }
  // get admin profile
  async getInstitute() {
    const {data} = await http.get(`/supreme/account/v1/dashboard-data`);
    return data;
  }
}
export default new Account();
