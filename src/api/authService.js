import axios from "axios";
import http from "./common";
import { useRouter } from "vue-router";
import { useStore } from "vuex";
import { useToast } from "vue-toastification";
const router = useRouter();
const store = useStore();
const toast = useToast();
// import dotenv from 'dotenv'
// dotenv.config()
const API_URL = "https://app.lcgapp.online/api-v2022";
//  const API_URL = "http://localhost:4000/api-v2022";
class AuthService {
  // login
  async login(user) {
   return await axios.post(API_URL + "/supreme/auth/v1/login", {
      email: user.email,
      password: user.password,
    });
  }
  
  
  // logout
  async logout() {
    localStorage.removeItem("lcg_token");
    toast.success("Loggout Successfully!");
  }
  // Forget Password
  async forgetPass(user){
    const {data} = await http.post("/supreme/auth/v1/forgot-password",{
      email:user.email
    });
    return data;
  }
  // Forget Password
  async resetPass(user){
    const {data} = await http.post("/supreme/auth/v1/reset-password",{
      email:user.email,
      otp:user.otp,
      password:user.password
    });
    return data;
  }
  //check activation link
  async checkActivationKey(key) {
    return await http.post("/staff/auth/v1/activate-account", {
      activationKey: key,
    });
  }
  // activate account
  async activateAccount(param) {
    console.log("param", param);
    const { data } = await http.post("/staff/auth/v1/activate-account", {
      activationKey: param.key,
      password: param.password,
    });
    return data;
  }
}
export default new AuthService();