import { createRouter, createWebHistory } from "vue-router";
import Style from "@/views/Style.vue";
import store from '../store';
import AuthService from "@/api/authService";
 const loadComponent =(view)=>{
   return ()=> import(/* webpackChunkName: "reset" */ `../views/Admin/${view}.vue`);
 }
const router = createRouter({
  history: createWebHistory(),
   routes : [
    {
      meta: {
        title: "Select style",
        fullScreen: true,
      },
      path: "/style",
      name: "style",
      component: Style,
    },
    {
      meta: {
        title: "Activation",
        fullScreen: true,
      },
      path: "/staff/activate-account/:key",
      name: "activation",
      component: () =>
        import(/* webpackChunkName: "reset" */ "@/views/Activation.vue"),
    },
    {
      meta: {
        title: "Otp",
        fullScreen: true,
      },
      path: "/otp",
      name: "otp",
      component: () => import(/* webpackChunkName: "reset" */ "@/views/Otp.vue"),
    },
    {
      meta: {
         AuthService:true,
        title: "Dashboard",
      },
      path: "/dashboard",
      name: "dashboard",
      component: loadComponent('Dashboard'),
    },
    {
      meta: {
         AuthService:true,
        title: "Hospital List",
      },
      path: "/hospital-list",
      name: "hospital-list",
  
      component: () =>
        import(
          /* webpackChunkName: "tables" */ "@/views/Hospitals/HospitalList.vue"
        ),
    },
  
    {
      meta: {
        AuthService:true,
        title: "Profile",
      },
      path: "/profile",
      name: "profile",
      component: loadComponent('Profile'),
    },
  
    {
      meta: {
        title: "Login",
        fullScreen: true,
        AuthService:false,
      },
      path: "/",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "@/views/Login.vue"),
    },
    {
      meta: {
        title: "Forget Password",
        fullScreen: true,
      },
      path: "/forget-password",
      name: "forget-password",
      component: () =>
        import(/* webpackChunkName: "forget" */ "@/views/ForgetPassword.vue"),
    },
    {
      meta: {
        title: "Sample",
        fullScreen: true,
      },
      path: "/sample",
      name: "sample",
      component: () =>
        import(/* webpackChunkName: "forget" */ "@/views/sample.vue"),
    },
    {
      meta: {
        title: "Reset Password",
        fullScreen: true,
      },
      path: "/reset-password",
      name: "reset-password",
      component: () =>
        import(/* webpackChunkName: "reset" */ "@/views/ResetPassword.vue"),
    },
    {
      meta: {
        AuthService:true,
        title: "Add Hospital",
      },
      path: "/add-hospital",
      name: "add-hospital",
      component: () =>
        import(
          /* webpackChunkName: "add hospital" */ "@/views/Hospitals/AddHospital.vue"
        ),
    },
    {
      meta: {
        AuthService:true,
        title: "Edit Hospital",
      },
      path: "/edit-hospital/:instituteId",
      name: "edit-hospital",
      component: () =>
        import(
          /* webpackChunkName: "edit hospital" */ "@/views/Hospitals/EditHospital.vue"
        ),
    },
    {
      meta: {
        AuthService:true,
        title: "Hospital Detail",
      },
      path: "/hospital-detail/:instituteId",
      name: "hospital-detail",
      component: () =>
        import(
          /* webpackChunkName: "hospital Details" */ "@/views/Hospitals/HospitalDetail.vue"
        ),
    },
    {
      meta: {
        title: "Staff Detail",
      },
      path: "/staff-detail/:instituteId/:staffId",
      name: "staff-detail",
      component: () => import("@/views/Staffs/StaffDetail.vue"),
    },
    {
      meta: {
        title: "Staff List",
      },
      path: "/hospital/:instituteId",
      name: "staff-list",
      component: () =>
        import(
          /* webpackChunkName: "staff & ward list" */ "@/views/Staffs/StaffList.vue"
        ),
  
      children: [
        {
          path: "staff-list/:instituteId",
          name: "StaffCard",
          component: () =>
            import(
              /* webpackChunkName: "staff card" */ "@/components/StaffCard.vue"
            ),
        },
        {
          path: "ward/:instituteId",
          name: "WardCard",
          component: () =>
            import(
              /* webpackChunkName: "ward card" */ "@/components/WardCard.vue"
            ),
        },
      ],
    },
  
    {
      meta: {
        title: "Add Staff",
      },
      path: "/add-staff/:instituteId",
      name: "add-staff",
      component: () =>
        import(
          /* webpackChunkName: "add stafff" */ "@/views/Staffs/AddStaff.vue"
        ),
    },
    {
      meta: {
        title: "Edit Staff",
      },
      path: "/edit-staff/:instituteId/:staffId",
      name: "edit-staff",
      component: () => import("@/views/Staffs/EditStaff.vue"),
    },
  
    {
      meta: {
        AuthService:true,
        title: "Edit Profile",
      },
      path: "/edit-profile",
      name: "edit-profile",
      component: () =>
        import(
          /* webpackChunkName: "edit profile" */ "@/views/Admin/EditProfile.vue"
        ),
    },
    
  ]
});
router.beforeEach((to, from, next)=>{
  if(to.meta.AuthService){
    let token = localStorage.getItem('lcg_token');
    if (store.state.isAuthenticated && token.length>0) {
      next();
    }
    else{
      next('/');
    }
  }else{
    next( );
  }
})
export default router;
