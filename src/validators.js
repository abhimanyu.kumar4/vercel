export function validName(name) {
  let validNamePattern = new RegExp("^[a-zA-Z]+(?:[-'\\s][a-zA-Z]+)*$");
  if (validNamePattern.test(name)){
    return true;
  }
  return false;
}

export function validEmail(email) {
  let validEmailPattern = new RegExp("/[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/g");
  if (validEmailPattern.test(email)){
    return true;
  }
  return false;
}
export function validPassword(password) {
  let validPasswordPattern = new RegExp("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,16}$");
  if (validPasswordPattern.test(password)){
    return true;
  }
  return false;
}