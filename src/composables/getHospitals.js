import { ref } from '@vue/reactivity'

const getHospitals =()=>{

    const hospitals = ref([])
    const error=ref(null)

    const load = async ()=>{
      try{
        let data = await fetch('http://localhost:3000/hospitals')
        if(!data.ok){
          throw Error('no available data')
        }

        hospitals.value= await data.json()

      }catch(err){
        error.value = err.message
        console.log(error.value);

      }
    }

    //load() // initiate 
     return{ hospitals, error, load}
}

export default getHospitals